# Suivi Fouling CTD POEM/SOLA

Script qui à pour fonction de suivre l'évolution de la qualité des données fournies par les bouées instrumentées POEM/SOLA, de produire un rapport automatique sur la qualité de ces données et d'alerter par mail sous certaines conditions de dégradation de qualité des données.

## Installation

+ [Instruction de déploiment](DEPLOY.md) 

## TODO
+ [ ] refaire un site propre qui peut accueillier sola et poem
+ [ ] faire une partie log/status sur le site pour verifier simplement le bon fonctionnement du system
+ [x] ajouter une partie détails avec le compte des différents anomalies
    + [ ] fix la mise à jour du détail
+ [ ] ajouter une description détails de la signification de charque tableau/graph/valeur
+ [ ] stocker les anomalies par catégorie ?
+ [ ] ajouter une page d'accueil facile à lire juste pour connaitre le status /dashboard ?
+ [ ] améliorer graphique
    + [ ] mettre le mois en lettre
    + [ ] faire commencer les ticks à 00:00
    + [ ] ajouter une séparation pour les jours et un label pour identifier les jours
    + [ ] fix le chevauchement des yticks de droite
+ [ ] faire des beaux mails et ressoudre l'encodage des caractére des mails
+ [ ] corriger le nombre de décimal dans le tableau, 1 decimal est largement suffisant

## Current work
rebuild report manager from new anomalies counts