#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 12 12:09:28 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""

# Python program to explain os.getresuid() method 
  
# importing os module 
import os, getpass
  
# Get the current process’s 
# real, effective, and saved user ids.
# using os.getresuid() method
ruid, euid, suid = os.getresuid()
  
# Print the current process’s
# real, effective, and saved user ids.
print("Real user id of the current process:", ruid)
print("Effective user id of the current process:", euid)
print("Saved user id of the current process:", suid)

print(f"Effective user is {getpass.getuser()}")