# Suivi Anomalies CTD : Installation

## Base de données

### Installation

Pour ubuntu et similaire :
`sudo apt install mysql-server`

https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04

Pour armhf (raspberry et beaglebone) :
`sudo apt install mariadb-server`

https://raspberry-pi.fr/installer-mariadb-raspbian/

Si besoin, relancer la base de données:
`sudo systemctl restart mysql.service`

Verifier que la base de données est bien active:
`sudo systemctl status mysql.service`

### Ajout d'un utilisateur
Avec les identifiants suivant :
    user : coasthf
    mdp : coasthf66
    db : coasthf
    
Connection en tant que root :
`sudo mysql`
Même sans installer mysql on peut utiliser la commande `mysql` pour se connecter à la base de donnée. Ca à l'air d'être grosso modo SQL compatible, donc pas trop de différences à l'usage.

Ajout d'un nouvel utilisateur et création de la base de donnée `coasthf` :
```
CREATE USER 'coasthf'@'localhost' IDENTIFIED BY 'coasthf66';
CREATE DATABASE coasthf default character set utf8;
GRANT ALL PRIVILEGES ON *.* TO 'coasthf'@'localhost' WITH GRANT OPTION;
```

Connection en tant que nouvel utilisateur :
`mysql -u coasthf -p`

### Ajour des tables dans la base de donnée
Selection de la base de donnée:
`use coasthf;`

Ajout des différentes tables:
```
create table raw_sola_med1m (
    datetime datetime not null unique,
    temperature_degc DOUBLE(7, 2),
    salinity DOUBLE(7, 2),
    fluorescence_rfu DOUBLE(7, 2),
    turbidity_ntu DOUBLE(7, 2),
    oxygen_mgl DOUBLE(7, 2),
    primary key (datetime)
    );
    
create table raw_poem_med1m (
    datetime datetime not null unique,
    temperature_degc DOUBLE(7, 2),
    salinity DOUBLE(7, 2),
    fluorescence_rfu DOUBLE(7, 2),
    turbidity_ntu DOUBLE(7, 2),
    oxygen_mgl DOUBLE(7, 2),
    primary key (datetime)
    );
    
create table qc_poem_med1m (
    datetime datetime not null unique,
    temperature_degc SMALLINT(1),
    salinity SMALLINT(1),
    fluorescence_rfu SMALLINT(1),
    turbidity_ntu SMALLINT(1),
    oxygen_mgl SMALLINT(1),
    primary key (datetime)
    );
    
create table qc_sola_med1m (
    datetime datetime not null unique,
    temperature_degc SMALLINT(1),
    salinity SMALLINT(1),
    fluorescence_rfu SMALLINT(1),
    turbidity_ntu SMALLINT(1),
    oxygen_mgl SMALLINT(1),
    primary key (datetime)
    );

create table anomaly_sola_sum1d (
    datetime datetime not null unique,
    error_temperature_degc SMALLINT(4) not null,
    error_salinity SMALLINT(4) not null,
    error_fluorescence_rfu SMALLINT(4) not null,
    error_turbidity_ntu SMALLINT(4) not null,
    error_oxygen_mgl SMALLINT(4) not null,
    missing_temperature_degc SMALLINT(4) not null,
    missing_salinity SMALLINT(4) not null,
    missing_fluorescence_rfu SMALLINT(4) not null,
    missing_turbidity_ntu SMALLINT(4) not null,
    missing_oxygen_mgl SMALLINT(4) not null,
    primary key (datetime)
    );

create table anomaly_poem_sum1d (
    datetime datetime not null unique,
    error_temperature_degc SMALLINT(4) not null,
    error_salinity SMALLINT(4) not null,
    error_fluorescence_rfu SMALLINT(4) not null,
    error_turbidity_ntu SMALLINT(4) not null,
    error_oxygen_mgl SMALLINT(4) not null,
    missing_temperature_degc SMALLINT(4) not null,
    missing_salinity SMALLINT(4) not null,
    missing_fluorescence_rfu SMALLINT(4) not null,
    missing_turbidity_ntu SMALLINT(4) not null,
    missing_oxygen_mgl SMALLINT(4) not null,
    primary key (datetime)
    );
```

Afficher les tables nouvellement crées:
`show tables;`

Si besoin, l'insertion d'une ligne dans la base de donnée : 
`INSERT INTO raw_sola_med1m VALUES ('2022-09-02 00:00:40', 23.9, 38.0, 0.1, -0.8, 5.3);`

La base de donnée est maintenant utilisable.

## Environnement Python

Si python3 n'est pas déjà installé:
`sudo apt install python3`

Installer pip:
```
wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py 
```

Installer les packages nécessaires:
`python3 -m pip install numpy pandas requests`

Résolution de quelques problèmes sur beaglebone:

+ problème de charset avec mysql
https://stackoverflow.com/questions/73244027/character-set-utf8-unsupported-in-python-mysql-connector
`python3 -m pip install mysql-connector-python==8.0.29`

+ pandas qui met un temps fou à s'installer (finalement le laisser tournée une nuit ça marche aussi)
https://github.com/pandas-dev/pandas/issues/21289

+ problème avec matplotlib 
https://stackoverflow.com/questions/48012582/pillow-libopenjp2-so-7-cannot-open-shared-object-file-no-such-file-or-directo
`sudo apt-get install libopenjp2-7`


## Installation des scripts

Téléchargement des scripts : 
`git clone https://gitlab.com/suivifoulingctd/suivifoulingctd_backend.git`

Mise à jour des scripts : (commande à lancer dans le dossier créé lors du `git clone`)
`git pull origin main`

## Mise à jour website

Ce truc est seulement nécessaire si on veut se servir de gitlab pour l'hébergement du site.

+ faire un git clone avec login et mot de passe
`git clone https://<login>:<password>@gitlab.com/suivifoulingctd/suivifoulingctd.gitlab.io.git`
+ enjoy!


## Service systemd
Il existe plusieurs manières de lancer un script periodiquement. Une manière relativement robuste est d'utiliser le système de service et de timer de systemd sous linux. Cette application d'alerte fouling ctd utilise ainsi systemd pour être executer périodiquement.

Plus d'info sur systemd et la création de service sous systemd :
https://doc.ubuntu-fr.org/systemd
https://doc.ubuntu-fr.org/creer_un_service_avec_systemd

**Mise en place du service et du timer:**

Copier les fichiers de service du dossier `service_linux` au bon endroit dans le système hôte:
`/etc/systemd/system`

Dans le fichier .timer : modifier les heures de déclanchement du service 
`OnCalendar=*-*-* 07:30:00` permet de lancer le service tout les jours à 7h30.
Plus d'info : https://silentlad.com/systemd-timers-oncalendar-(cron)-format-explained

Dans le fichier .service : modifier l'utilisateur et le chemin du script
```
User=debian
Group=debian
ExecStart=/usr/bin/python3 /home/debian/suivifoulingctd_backend/main.py
```
Remplacer par le bon utilisateur, groupe et chemin du script.  Pour connaitre l'utilisateur et le groupe du script : `ls -l <le_chemin>/suivifoulingctd_backend`

Verifier que les services sont bien possedés par l'utilisateur `root`:
`ls -l /etc/systemd/system`

Activer le timer:
`sudo systemctl enable suivifoulingctd.timer`

Lancer le timer:
`sudo systemctl start suivifoulingctd.timer`

Verifier le status des services:
`sudo systemctl status suivifoulingctd.timer`
`sudo systemctl status suivifoulingctd.service`

Affichage de logs:
`journalctl -u suivifoulingctd.service`

Recharger les services après modification:
`sudo systemctl daemon-reload`

## Envoi de mail

Il y avait un soucis de protocol pas supporté
https://stackoverflow.com/questions/32330919/python-ssl-ssl-sslerror-ssl-unsupported-protocol-unsupported-protocol-ssl

C'est probablement une fail de sécurité, à réparé autrement de péférence