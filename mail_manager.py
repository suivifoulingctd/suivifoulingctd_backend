#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  6 01:26:45 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""

#https://realpython.com/python-send-email/
#https://pypi.org/project/emails/

import smtplib, ssl
import config
import pandas as pd

host = config.server
port = config.port
username = config.username
password = config.password

sendermail = config.sendermail
receivermail = "thierryvala@hotmail.fr"

message = """From: <{sendermail}>
To: <{receivermail}>
Subject: {subject}

{body}
"""

def sendmail(to,subject,body):
    context = ssl.create_default_context()
    with smtplib.SMTP(host, port) as server:
        server.ehlo()  # Can be omitted
        server.starttls(context=context)
        server.ehlo()  # Can be omitted
        server.login(username, password)
        server.sendmail(sendermail, to, message.format(sendermail=sendermail,receivermail=to,subject=subject,body=body))
        
if __name__ == '__main__':
    sendmail(receivermail,"It's working!", "This is an automated message. You can respond but I won't read it. https://realpython.com/python-send-email/.")
    
    # now = pd.Timestamp.now().strftime("%Y-%m-%d %H:%M:%S")
    # sendmail(["thierryvala@hotmail.fr","thierry.vala@ecomail.fr"],f"Test [{now}]",f"Test [{now}]")