#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 02:37:30 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""

import logging_manager as lm
logger = lm.getlogger(__name__)

import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

#utctimestamp_yyyy-mm-dd_hh:mm:ss,temperature_degc,salinity,fluorescence_rfu,turbidity_ntu,oxygen_mgl
#2020-01-01 00:00:00,13.8,36.8,0.1,2.3,6.3

def ctd(fname,add_mdates=True,nrows=None):
    logger.info("parsing ctd file and checking if it's the right format")
    dateparse = lambda X: datetime.datetime.strptime(X,'%Y%m%d%H%M%S')
    data = pd.read_table(fname,header=0,
                        decimal='.',
                        sep = ",",
                        usecols=[0,1,2,3,4,5],
                        names=["﻿utctimestamp","temperature_degc","salinity","fluorescence_rfu","turbidity_ntu","oxygen_mgl"],
                        parse_dates={"datetime": ["﻿utctimestamp"]},
                        date_parser=dateparse,
                        nrows=nrows
                        #delim_whitespace = True,
                        #skip_blank_lines=True
                        )
    if add_mdates :
        data.loc[:,"mdates"] = [mdates.date2num(e) for e in data.datetime]
    return data

def ctds(*fnames,add_mdates=True):
    logger.info("parsing multiple ctd file and checking if it's the right format")
    data = ctd(fnames[0],False)
    for fname in fnames[1:]:
        data = pd.concat((data,ctd(fname,False)))
    data.reset_index(drop=True,inplace=True)
    if add_mdates :
        data.loc[:,"mdates"] = [mdates.date2num(e) for e in data.datetime]
    return data