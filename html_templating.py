#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  4 21:55:37 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""

html_template = """
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Suivie CTD {buoy_name}</title>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="HTML5 Example Page">
  <link rel="stylesheet" href="styles.css">
</head>
<body>
  <header>
    <h1>Suivie Qualité des données CTD {buoy_name}</h1>
    <h2>{date}</h2>
    <h3>Date génération page : {build_date}</h3>
  </header>

<h2>Anomalies par capteur pour le {date} (%)</h2>
{table}

<h2> Données brutes {buoy_name} pour les 7 derniers jours avec anomalies </h2>

{graph}

<h2> Détails des anomalies détectées pour le {date}</h2>
{detail_table} 	

  <footer>
    <p>Université Perpignan Via Domitia. Laboratoire Océanologique de Banuyls sur Mer.</p>
   	<p>Auteur : thierryvala@etudiant.univ-perpi.fr</p>
  </footer>

  <script src="/assets/js/scripts.js"></script>
</body>
</html>
"""

html_params = {
    "buoy_name" : "<buoy name placeholder>",
    "date" : "<date placeholder>",
    "table" : "<table placeholder>",
    "graph" : "<graph placeholder>"
    }

td_template = '<td class="value {value_color}">{value}% <div class="variation {var_color}">{arrow}({var}%)</div></td>'

td_params = {
    "value_color" : "",
    "value" : "",
    "var_color" : "",
    "arrow" : "",
    "var" : ""
    }

table_template = """
<table>
  <tr>
    <th>Capteurs : </th>
    <th>Température</th>
    <th>Salinité</th>
    <th>Fluorescence</th>
    <th>Turbidité</th>
    <th>Oxygène</th>
  </tr>
  <tr>
    <td>Anomalies</td>
    {error}
  </tr>
  <tr>
    <td>Manquantes</td>
    {missing}
  </tr>
</table> 
"""

table_params = {
    "error" : "",
    "missing" : ""
    }

img_template = '<img src="{img}"></img>'

arrows = {
    "up" : "&#9650;",
    "down" : "&#9660;",
    "stable" : "&#9632;"
    }

#print(html_template.format(**params))

