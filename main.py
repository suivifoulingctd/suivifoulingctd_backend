#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  4 03:41:55 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""
# =============================================================================
# SCRIPT : CTD Fouling alert
# =============================================================================

#
# Purpose : send alerts when there is to much fouling on the sensor of the buoy
#

# =============================================================================
# IMPORTS
# =============================================================================
import logging_manager as lm
import sys
import numpy as np
import pandas as pd

import database_manager as dbm
import load_ctd
#import anomaly_count as anoc
#import report_manager
import report_manager2 
import website_manager as wm
import alert_manager as am
import file_manager as fm 
import config

# =============================================================================
# CONFIG
# =============================================================================
buoy_name = "sola"
logger = lm.getlogger(__name__)

def run():
    # =============================================================================
    # MAIN SCRIPT 
    # =============================================================================
    # try to connect to the database to see if it works
    try: 
        dbm.get_db_cursor()
    except Exception:
        logger.error(f"Can't connect to the database.")
        #sys.exit()
        
    # check if there is already a record for yesterturday, if yes quit,
    # if not proceed
    if dbm.is_up_to_date(buoy_name) :
        # db up to date
        logger.info(f"Database for {buoy_name} raw data is up to date. Skipping ...")
        #sys.exit()
    else :
        # db not up to date
        # load data from website with check of reachable and such
        try :
            data = load_ctd.load_ctd(load_ctd.get_url(buoy_name))
            dbm.insert_raw_data(buoy_name,data)
        except Exception:
            logger.error(f"Can't pull the {buoy_name} ctd data from the server.")

        # save raw data to db
        

        # calculate anomalies and save it to db
        #anomaly_data = anoc.count_anomalies(buoy_name, data)
        # now count_anomalies is called by build_report in report manager

    # build and put report online with boy status
    report_manager2.build_report(buoy_name)

    #put the report online
    if config.productionrun :
        wm.gitlab_update()
    
    # send alert by mail if needed
    #am.anomaly_watcher(buoy_name)
    # it's probably broken, need to fix this

if __name__ == '__main__' :
    run()
