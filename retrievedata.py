#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 12 11:14:41 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""

import pandas as pd
import numpy as np
import mysql.connector
import config

db = mysql.connector.connect(
  host="beaglebone.local",
  user=config.user,
  password=config.pwd,
  database=config.dbname
)
cursor = db.cursor()