#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  4 15:52:15 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""

#logging
import logging_manager as lm
logger = lm.getlogger(__name__)

#imports
import load_ctd
import numpy as np
import pandas as pd
import database_manager as dbm
import file_manager as fm
import config
import tools as tl

class ANOMALY:
    def __init__(self,
                 datetime,
                 mdates,
                 f_badflag,
                 f_startflag,
                 f_null,
                 f_sl,
                 f_sp,
                 f_sk,
                 anoms,
                 sum_rejected):
        self.datetime = datetime
        self.mdates = mdates
        self.f_badflag = f_badflag
        self.f_startflag = f_startflag
        self.f_null = f_null
        self.f_sl = f_sl
        self.f_sp = f_sp
        self.f_sk = f_sk
        self.anoms = anoms
        self.sum_rejected = sum_rejected

def count_anomalies(buoy_name,data,dtcol="datetime",mdcol="mdates"):
    # =============================================================================
    # SANITY CHECKS
    # =============================================================================
    logger.info("run some sanity checks on function entry parameters")
    if not buoy_name in config.buoy_list:
        # checks if the buoy is available in the list of buoys
        raise ValueError(f"Expect buoy to be in {config.buoy_list}")
    if not tl.islistinlist(config.sensor_list[buoy_name], list(data.keys())):
        # checks if the right columns are available in the data 
        raise ValueError(f"Expect data to contains at least the columns : {config.sensor_list[buoy_name]}")
    
    # =============================================================================
    # INIT
    # =============================================================================
    logger.info("init some variables")
    keys = config.sensor_list[buoy_name]
    staticmin = config.staticmin
    staticmax = config.staticmax
    th_sl = config.th_sl
    th_sp = config.th_sp
    window = config.window
    threshold_factor = config.threshold_factor
    badflag = config.badflag
    startflag = config.startflag    
    
    f_end = pd.DataFrame(True,index=data.index,columns=keys)
    f_end.iloc[[0,-1]] = False
    
    # =============================================================================
    # THRESHOLDS config for med1m
    # =============================================================================
    # threshold are in the config file!
    
    # =============================================================================
    # DROP ERRROR VAL 
    # =============================================================================
    f_badflag = (data[keys] == badflag)
    f_startflag = (data[keys] == startflag)
    
    # =============================================================================
    # BASIC TESTS : Static (impossible values test), Slope and Spike
    # =============================================================================
    
    logger.info("compute basic tests")
    
    # create 3 dataframes shifted of -1,0 and +1 time wise
    x1 = data[keys] # x_{i}
    x0 = data[keys].shift(1) # x_{i-1}
    x2 = data[keys].shift(-1) # x_{i+1}
    
    #compute slope (it's an increase actually, not a slope as we don't involve time)
    sl = ((x2 - x0).abs())/2
    #compute spike
    sp = ((x1 - (x2 + x0)/2).abs() - ((x2 - x0).abs())/2).abs()
    
    # get a filter for the null values
    f_null = data[keys].isnull()
    
    #test impossible values : "ge" means "greater than of equal" and "le" means "less than or equal" 
    f_static = data[keys].ge(staticmin) & data[keys].le(staticmax)
    
    #test slope and spike
    f_sl = sl.lt(th_sl)
    f_sp = sp.lt(th_sp)
    
    # =============================================================================
    # ADAPTATIVE TEST
    # =============================================================================
    
    logger.info("compute adaptative tests")
    
    # interpolate missing values with last non null value,
    # rolling windows doesn't like missing value,
    # it's just to compute the adaptative test
    interp = data[keys].interpolate()
    
    # rolling centered mean
    mean = interp.rolling(window,center=True,min_periods=1).mean()
    
    # get the HF part of the signal, substracting a rolling mean kinda does that
    hf = interp - mean
    
    # rolling standard deviation on the HF part of the signal
    std = hf.rolling(window,center=True,min_periods=1).std()
    
    # "threshold" for the adaptative test, it's a moving threshold
    offset = std*threshold_factor
    
    # compute adaptative test : see documentation
    f_sk = (hf > -offset) & (hf < +offset) | (hf.abs() < hf.std()*threshold_factor)
        
    # =============================================================================
    # COMPUTE ANOMALY
    # =============================================================================
        
    logger.info("compute anomalies array for each sensor")
    
    # table of anomalies : True is anomaly, false is not an anomaly
    
    # anoms = (~f_static | ~f_sl | ~f_sp | ~f_sk) & ~f_null & ~f_badflag & ~f_startflag & f_end
    # disable adaptative test
    anoms = (~f_static | ~f_sl | ~f_sp) & ~f_null & ~f_badflag & ~f_startflag & f_end
    
    # =============================================================================
    # FORMAT REJECTED
    # =============================================================================
    
    logger.info("sum rejected values for each test and each sensor")
    
    f_untracked = f_null | f_badflag | f_startflag # value not to count in the tests
    f_static_ = ~f_static & ~f_untracked
    f_sl_ = ~f_sl & ~f_untracked & f_end
    f_sp_ = ~f_sp & ~f_untracked & f_end
    f_sk_ = ~f_sk & ~f_untracked & f_end
    
    # disable startflag, badflag and adaptative test
    # sum_rejected = pd.concat((f_badflag.sum(),f_startflag.sum(),f_null.sum(),f_static_.sum(),f_sl_.sum(),f_sp_.sum(),f_sk_.sum(),anoms.sum()),axis=1,keys=["badflag","startflag", "null", "rstatic", "rsl", "rsp", "rsk", "anoms"])
    sum_rejected = pd.concat((f_null.sum(),f_static_.sum(),f_sl_.sum(),f_sp_.sum(),anoms.sum()),axis=1,keys=["null", "rstatic", "rsl", "rsp", "anoms"])
    
    anomaly = ANOMALY(datetime = data[dtcol],
                      mdates = data[mdcol],
                      f_startflag = f_startflag,
                      f_badflag = f_badflag,
                      f_null = f_null,
                      f_sl = f_sl,
                      f_sp = f_sp,
                      f_sk = f_sk,
                      anoms = anoms,
                      sum_rejected = sum_rejected)
    
    return anomaly
    
if __name__ == '__main__' :
    buoy_name = "sola"
    data = load_ctd.load_ctd(load_ctd.get_url(buoy_name))
    r = count_anomalies(buoy_name, data)