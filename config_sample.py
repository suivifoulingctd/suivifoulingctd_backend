#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This file should be modified to match the configuration of the system on
# which the script is running on. 

# =============================================================================
# GLOBAL CONFIG
# =============================================================================
buoy_list = ["sola"] # add poem when it's available
productionrun = False # define if it's a test run or a production run

# =============================================================================
# EMAIL CONFIG
# =============================================================================
server = ""
port = 587
username = ""
password = ""
sendermail = ""

debug_mail_list = [""] 
info_mail_list = ["",""]

# =============================================================================
# REPORT CONFIG
# =============================================================================
# careful, it's system dependant
public_basepath = ""
web_basepath = ""
archive_basepath = ""

# =============================================================================
# UPDATE CONFIG
# =============================================================================
# careful, it's system dependant
server_basepath = ""

# =============================================================================
# LOG CONFIG
# =============================================================================
# careful, it's system dependant
log_basepath = ""

# =============================================================================
# DATABASE CONFIG
# =============================================================================

# thats's a configuration of your database mysql or mariadb
host=""
user=""
pwd = ""
dbname = ""
