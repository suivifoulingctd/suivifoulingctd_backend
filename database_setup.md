# MySQL database setup

https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04
https://vegibit.com/creating-databases-and-tables-in-mysql/
https://www.w3schools.com/python/python_mysql_create_db.asp
https://docs.sqlalchemy.org/en/14/intro.html#installation
https://docs.phpmyadmin.net/en/latest/setup.html#upgrading-from-an-older-version



host : localhost
user : coasthf
mdp : coasthf66

as root :

sudo mysql

SELECT user,authentication_string,plugin,host FROM mysql.user;
CREATE USER 'coasthf'@'localhost' IDENTIFIED BY 'coasthf66';
CREATE DATABASE coasthf default character set utf8;
GRANT ALL PRIVILEGES ON *.* TO 'coasthf'@'localhost' WITH GRANT OPTION;

as coasthf:

mysql -u coasthf -p

use coasthf;
show tables;

# CREATE TABLES

create table raw_sola_med1m (
    datetime datetime not null unique,
    temperature_degc DOUBLE(7, 2),
    salinity DOUBLE(7, 2),
    fluorescence_rfu DOUBLE(7, 2),
    turbidity_ntu DOUBLE(7, 2),
    oxygen_mgl DOUBLE(7, 2),
    primary key (datetime)
    );
    
create table raw_poem_med1m (
    datetime datetime not null unique,
    temperature_degc DOUBLE(7, 2),
    salinity DOUBLE(7, 2),
    fluorescence_rfu DOUBLE(7, 2),
    turbidity_ntu DOUBLE(7, 2),
    oxygen_mgl DOUBLE(7, 2),
    primary key (datetime)
    );
    
create table qc_poem_med1m (
    datetime datetime not null unique,
    temperature_degc SMALLINT(1),
    salinity SMALLINT(1),
    fluorescence_rfu SMALLINT(1),
    turbidity_ntu SMALLINT(1),
    oxygen_mgl SMALLINT(1),
    primary key (datetime)
    );
    
create table qc_sola_med1m (
    datetime datetime not null unique,
    temperature_degc SMALLINT(1),
    salinity SMALLINT(1),
    fluorescence_rfu SMALLINT(1),
    turbidity_ntu SMALLINT(1),
    oxygen_mgl SMALLINT(1),
    primary key (datetime)
    );

create table anomaly_sola_sum1d (
    datetime datetime not null unique,
    error_temperature_degc SMALLINT(4) not null,
    error_salinity SMALLINT(4) not null,
    error_fluorescence_rfu SMALLINT(4) not null,
    error_turbidity_ntu SMALLINT(4) not null,
    error_oxygen_mgl SMALLINT(4) not null,
    missing_temperature_degc SMALLINT(4) not null,
    missing_salinity SMALLINT(4) not null,
    missing_fluorescence_rfu SMALLINT(4) not null,
    missing_turbidity_ntu SMALLINT(4) not null,
    missing_oxygen_mgl SMALLINT(4) not null,
    primary key (datetime)
    );

create table anomaly_poem_sum1d (
    datetime datetime not null unique,
    error_temperature_degc SMALLINT(4) not null,
    error_salinity SMALLINT(4) not null,
    error_fluorescence_rfu SMALLINT(4) not null,
    error_turbidity_ntu SMALLINT(4) not null,
    error_oxygen_mgl SMALLINT(4) not null,
    missing_temperature_degc SMALLINT(4) not null,
    missing_salinity SMALLINT(4) not null,
    missing_fluorescence_rfu SMALLINT(4) not null,
    missing_turbidity_ntu SMALLINT(4) not null,
    missing_oxygen_mgl SMALLINT(4) not null,
    primary key (datetime)
    );

# INSERT

INSERT INTO _table_name_  
VALUES (_value1_, _value2_, _value3_, ...);
INSERT INTO raw_sola_sum1d VALUES ('2022-09-02 00:00:40', 23.9, 38.0, 0.1, -0.8, 5.3);



