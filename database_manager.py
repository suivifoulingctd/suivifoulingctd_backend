#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep  3 00:51:32 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""

#https://popsql.com/learn-sql/mysql/how-to-query-date-and-time-in-mysql

import logging_manager as lm
logger = lm.getlogger(__name__)

import pandas as pd
import numpy as np
import mysql.connector

import tools as tl
import load_ctd
import config

def is_buoy_available(buoy_name):
    if buoy_name in config.buoy_list:
        return True
    else :
        print(f"buoy name {buoy_name} unkown")
        
def db_query(request, param):
    db = mysql.connector.connect(
        host=config.host,
        user=config.user,
        password=config.pwd,
        database=config.dbname
    )
    cursor = db.cursor()
    if type(param) == list :
        cursor.executemany(request, param)
    elif type(param) == tuple :
        cursor.execute(request, param)
    else :
        raise TypeError("Given param is the wrong type, expecting list or tuple")
    db.commit()
    print(cursor.rowcount, "record inserted.")
    
def get_db_cursor():
    try:
        db = mysql.connector.connect(
          host=config.host,
          user=config.user,
          password=config.pwd,
          database=config.dbname
        )
        cursor = db.cursor()
        return cursor
    except mysql.connector.DatabaseError as dbe:
        print(f"DatabaseError : {dbe.msg}")
        raise dbe
    except Exception as ex:
        print("DatabaseError : Unkown issue, can't connect to the database.")
        raise ex
    

# insert today raw data for sola or poem
# there is also some checks
def insert_raw_data(buoy, df):
    # check if the buoy is available
    if not is_buoy_available(buoy):
        raise ValueError(f"Expect buoy_name to be in {config.buoy_list}")
    else :
        table_name = f"raw_{buoy}_med1m"
    
    # checking if there is only one day worth of data
    if not df.datetime.sort_values().iloc[-1].day == df.datetime.sort_values().iloc[0].day :
        raise ValueError("Expect df to contain 1 day worth of data at maximum. ")
    
    # checking if there is the good columns
    # will thorw an error if a column is missing
    tmpdata = df[['datetime',
     'temperature_degc',
     'salinity',
     'fluorescence_rfu',
     'turbidity_ntu',
     'oxygen_mgl']]
                  
    # format the data for inserting in databasedata = data.drop("mdates",axis=1)
    # datetime in string format
    tmpdata.datetime = pd.to_datetime(tmpdata.datetime).dt.strftime("%Y-%m-%d %H:%M:%S")
    # make a list of tuples
    vals = [tuple(r) for r in tmpdata.to_numpy()]
    
    # build the request
    sql = f"INSERT INTO {table_name} VALUES (%s, %s, %s, %s, %s, %s);"
    
    # query db
    db_query(sql, vals)
    
def insert_qc_data(buoy, df):
    # check if the buoy is available
    if not is_buoy_available(buoy):
        raise ValueError(f"Expect buoy_name to be in {config.buoy_list}")
    else :
        table_name = f"qc_{buoy}_med1m"
    
    # checking if there is only one day worth of data
    if not df.datetime.sort_values().iloc[-1].day == df.datetime.sort_values().iloc[0].day :
        raise ValueError("Expect df to contain 1 day worth of data at maximum. ")
    
    # checking if there is the good columns
    # will thorw an error if a column is missing
    tmpdata = df[['datetime',
     'temperature_degc',
     'salinity',
     'fluorescence_rfu',
     'turbidity_ntu',
     'oxygen_mgl']]
                  
    # format the data for inserting in databasedata = data.drop("mdates",axis=1)
    # datetime in string format
    tmpdata.datetime = pd.to_datetime(tmpdata.datetime).dt.strftime("%Y-%m-%d %H:%M:%S")
    # make a list of tuples
    vals = [tuple(r) for r in tmpdata.to_numpy()]
    
    # build the request
    sql = f"INSERT INTO {table_name} VALUES (%s, %s, %s, %s, %s, %s);"
    
    # query db
    db_query(sql, vals)
 
# insert today anomaly data for sola or poem                  
def insert_anomaly_data(buoy, df):
    # check if the buoy is available
    if not is_buoy_available(buoy):
        raise ValueError(f"Expect buoy_name to be in {config.buoy_list}")
    else :
        table_name = f"anomaly_{buoy}_sum1d"
    
    # checking if there is only one day worth of data
    if not df.datetime.sort_values().iloc[-1].day == df.datetime.sort_values().iloc[0].day :
        raise ValueError("Expect df to contain 1 day worth of data at maximum. ")
    
    # checking if there is the good columns
    # will thorw an error if a column is missing
    tmpdata = df[['datetime',
    'error_temperature_degc',
    'error_salinity',
    'error_fluorescence_rfu',
    'error_turbidity_ntu',
    'error_oxygen_mgl',
    'missing_temperature_degc',
    'missing_salinity',
    'missing_fluorescence_rfu',
    'missing_turbidity_ntu',
    'missing_oxygen_mgl']]
    
    # format the data for inserting in databasedata = data.drop("mdates",axis=1)
    # datetime in string format
    tmpdata.datetime = pd.to_datetime(tmpdata.datetime).dt.strftime("%Y-%m-%d %H:%M:%S")
    # make a list of tuples
    vals = [tuple(r) for r in tmpdata.to_numpy()]
    
    # build the request
    sql = f"INSERT INTO {table_name} VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
    
    # query db
    db_query(sql, vals)

#%%
# load last 7 days raw data for sola or poem
def load_raw_data_7d(buoy):
    if not is_buoy_available(buoy):
        raise ValueError(f"Expect buoy_name to be in {config.buoy_list}")
    else :
        table_name = f"raw_{buoy}_med1m"
    
    begin = (pd.Timestamp.now()-pd.Timedelta("7D")).floor("1D").strftime("%Y-%m-%d %H:%M:%S")
    end = ((pd.Timestamp.now()-pd.Timedelta("1D")).ceil("1D")-pd.Timedelta("1S")).strftime("%Y-%m-%d %H:%M:%S")
    
    db = mysql.connector.connect(
        host=config.host,
        user=config.user,
        password=config.pwd,
        database=config.dbname
    )
    cursor = db.cursor()
    
    sql = f"SELECT * FROM raw_{buoy}_med1m WHERE datetime BETWEEN '{begin}' AND '{end}'"
    
    cursor.execute(sql)
    
    r = cursor.fetchall()
    
    keys = ['datetime','temperature_degc','salinity','fluorescence_rfu','turbidity_ntu','oxygen_mgl']
    
    df = pd.DataFrame(np.asarray(r), columns = keys)
    df.datetime = pd.to_datetime(df.datetime)

    return df

# def load_raw_data(buoy,nb_days=1):
#     if not is_buoy_available(buoy):
#         raise ValueError(f"Expect buoy_name to be in {config.buoy_list}")
#     else :
#         table_name = f"raw_{buoy}_med1m"
    
#     begin = (pd.Timestamp.now()-pd.Timedelta(f"{str(nb_days)}D")).floor("1D").strftime("%Y-%m-%d %H:%M:%S")
#     end = ((pd.Timestamp.now()-pd.Timedelta("1D")).ceil("1D")-pd.Timedelta("1S")).strftime("%Y-%m-%d %H:%M:%S")
    
#     db = mysql.connector.connect(
#         host=config.host,
#         user=config.user,
#         password=config.pwd,
#         database=config.dbname
#     )
#     cursor = db.cursor()
    
#     sql = f"SELECT * FROM raw_{buoy}_med1m WHERE datetime BETWEEN '{begin}' AND '{end}'"
    
#     cursor.execute(sql)
    
#     r = cursor.fetchall()
    
#     keys = ['datetime','temperature_degc','salinity','fluorescence_rfu','turbidity_ntu','oxygen_mgl']
#     type_dict = {
#         'temperature_degc' : float,
#         'salinity' : float,
#         'fluorescence_rfu' : float,
#         'turbidity_ntu' : float,
#         'oxygen_mgl' : float
#         }
    
#     df = pd.DataFrame(np.asarray(r), columns = keys)
#     df.datetime = pd.to_datetime(df.datetime)
#     df = df.astype(type_dict)
#     df = tl.addmdates(df)
#     return df

def load_raw_data(buoy,begin,end):
    if not is_buoy_available(buoy):
        raise ValueError(f"Expect buoy_name to be in {config.buoy_list}")
    else :
        table_name = f"raw_{buoy}_med1m"
    
    db = mysql.connector.connect(
        host=config.host,
        user=config.user,
        password=config.pwd,
        database=config.dbname
    )
    cursor = db.cursor()
    
    sql = f"SELECT * FROM raw_{buoy}_med1m WHERE datetime BETWEEN '{begin}' AND '{end}'"
    
    cursor.execute(sql)
    
    r = cursor.fetchall()
    
    keys = ['datetime','temperature_degc','salinity','fluorescence_rfu','turbidity_ntu','oxygen_mgl']
    type_dict = {
        'temperature_degc' : float,
        'salinity' : float,
        'fluorescence_rfu' : float,
        'turbidity_ntu' : float,
        'oxygen_mgl' : float
        }
    
    df = pd.DataFrame(np.asarray(r), columns = keys)
    df.datetime = pd.to_datetime(df.datetime)
    df = df.astype(type_dict)
    df = tl.addmdates(df)
    return df

# load last 7 days qc codes data for sola or poem
def load_qc_data_7d(buoy):
    if not is_buoy_available(buoy):
        raise ValueError(f"Expect buoy_name to be in {config.buoy_list}")
    else :
        table_name = f"qc_{buoy}_med1m"
        
    begin = (pd.Timestamp.now()-pd.Timedelta("7D")).floor("1D").strftime("%Y-%m-%d %H:%M:%S")
    end = ((pd.Timestamp.now()-pd.Timedelta("1D")).ceil("1D")-pd.Timedelta("1S")).strftime("%Y-%m-%d %H:%M:%S")
    
    db = mysql.connector.connect(
        host=config.host,
        user=config.user,
        password=config.pwd,
        database=config.dbname
    )
    cursor = db.cursor()
    
    sql = f"SELECT * FROM qc_{buoy}_med1m WHERE datetime BETWEEN '{begin}' AND '{end}'"
    
    cursor.execute(sql)
    
    r = cursor.fetchall()
    
    keys = ['datetime','temperature_degc','salinity','fluorescence_rfu','turbidity_ntu','oxygen_mgl']
    
    df = pd.DataFrame(np.asarray(r), columns = keys)
    df.datetime = pd.to_datetime(df.datetime)

    return df

# load last 7 days anomaly data for sola or poem
def load_anomaly_data_7d(buoy):
    if not is_buoy_available(buoy):
        raise ValueError(f"Expect buoy_name to be in {config.buoy_list}")
    else :
        table_name = f"anomaly_{buoy}_sum1d"
        
    begin = (pd.Timestamp.now()-pd.Timedelta("7D")).floor("1D").strftime("%Y-%m-%d %H:%M:%S")
    end = ((pd.Timestamp.now()-pd.Timedelta("1D")).ceil("1D")-pd.Timedelta("1S")).strftime("%Y-%m-%d %H:%M:%S")
    
    db = mysql.connector.connect(
        host=config.host,
        user=config.user,
        password=config.pwd,
        database=config.dbname
    )
    cursor = db.cursor()
    
    sql = f"SELECT * FROM anomaly_{buoy}_sum1d WHERE datetime BETWEEN '{begin}' AND '{end}'"
    
    cursor.execute(sql)
    
    r = cursor.fetchall()
    
    keys = ['datetime','error_temperature_degc','error_salinity','error_fluorescence_rfu','error_turbidity_ntu','error_oxygen_mgl','missing_temperature_degc','missing_salinity','missing_fluorescence_rfu','missing_turbidity_ntu','missing_oxygen_mgl']
    
    df = pd.DataFrame(np.asarray(r), columns = keys)
    df.datetime = pd.to_datetime(df.datetime)

    return df

# get last recorded date for sola or poem
def get_last_recorded_date(buoy):
    if not is_buoy_available(buoy):
        raise ValueError(f"Expect buoy_name to be in {config.buoy_list}")
        
    db = mysql.connector.connect(
        host=config.host,
        user=config.user,
        password=config.pwd,
        database=config.dbname
    )
    
    cursor = db.cursor()
     
    sql = f"SELECT MAX(datetime) FROM raw_{buoy}_med1m;"
     
    cursor.execute(sql)
     
    r = cursor.fetchone()
    
    return pd.to_datetime(r[0])

def is_up_to_date(buoy):
    if not is_buoy_available(buoy):
        raise ValueError(f"Expect buoy_name to be in {config.buoy_list}")
    yesterday = (pd.Timestamp.now()-pd.Timedelta("1D")).day
    lastrec = get_last_recorded_date(buoy)    
    if lastrec == None :
        return False
    return yesterday == lastrec.day

if __name__ == '__main__' :
    if not is_up_to_date("sola") :
        print("not up to date")
        data = load_ctd.load_ctd(load_ctd.get_url("sola"))
        insert_raw_data("sola", data)
    else :
        print("up to date")