#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  6 21:58:25 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""

import logging_manager as lm
logger = lm.getlogger(__name__)

import database_manager as dbm
import tools as tl
import pandas as pd
import numpy as np
import mail_manager as mm

buoy_name = "sola"

def anomaly_watcher(buoy_name):
    anom_7d = dbm.load_anomaly_data_7d(buoy_name)
    anom_7d = tl.addmdates(anom_7d)
    anom_7d = anom_7d.sort_values(by="datetime")
    
    last_anom = anom_7d.iloc[-1]
    pasts_anom = anom_7d.iloc[:-1]
    
    errkeys = [e for e in list(anom_7d.keys()) if "error_" in e]
    
    nb1m = 24*60
    
    # last > 2 %
    cond1 = (last_anom[errkeys]/nb1m*100).values > 1
    # last.floor > pasts.max
    cond2 = np.floor((last_anom[errkeys]/nb1m*100).values) > np.max((pasts_anom[errkeys]/nb1m*100).values,axis=0)
  
    s_subject = "[Alerte CTD Fouling] - Seuils anoamlies depasses"
    s_msg_template = """
Bonjour,

Un certain nombre d'anomalies dans les donnees CTD de {buoy_name} a ete detecte :
{s_content}

Pour plus d'information vous pouvez vous connecter sur : https://suivifoulingctd.gitlab.io/
"""
    
    if (np.any(cond1) & np.any(cond2)):
        # send email alert
        print("email alert")
        logger.info("email alert to send")
        s_content = ""
        for i in range(len(errkeys)):
            if (cond1[i] & cond2[i]):
                print(f"{errkeys[i]} is over {np.floor((last_anom[errkeys]/nb1m*100).values)[i]}%!")
                s_content += f"\t+ {errkeys[i]} a depasse le seuil de {np.floor((last_anom[errkeys]/nb1m*100).values)[i]}%\n"
        
        s_msg = s_msg_template.format(buoy_name = buoy_name, s_content = s_content)
        
        mm.sendmail("thierryvala@hotmail.fr", s_subject, s_msg)
        
    else :
        print("no email alert")
        logger.info("no email alert to send")
        
if __name__ == '__main__':
    anomaly_watcher(buoy_name)
