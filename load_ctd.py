#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 02:37:30 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""

#https://stackoverflow.com/questions/54423838/python-logging-to-console

#http://observation.obs-banyuls.fr/acquisition/buoys/sola/alerts/ctdfouling.dat
#http://observation.obs-banyuls.fr/acquisition/buoys/poem/alerts/ctdfouling.dat

import requests
from io import StringIO
import read_ctd as ctd
import sys
from datetime import datetime
import config

# logging
import logging_manager as lm
logger = lm.getlogger(__name__)

# config
# // config.buoy_list
# config is in the config.py file!

# functions definition

def get_url(buoy_name):
    if buoy_name in config.buoy_list :
        return config.url_datafeeds[buoy_name]
    else :
        raise ValueError(f"expect buoy_name to be in {config.buoy_list}")

def load_ctd(url):
    r = requests.get(url)
    
    if r.status_code == 200 :
        # the url is reachable
        logger.info(f"online data file is reachable at {url}")
        try :
            # try to read is as a ctd file
            data = ctd.ctd(StringIO(r.text))
            logger.info(f"data is readable from online data file, it's the right format.")
        except:
            # is not readable as a ctd file
            # throw error
            logger.error(f"data is not readable from online data file, empty or wrong format.")
            sys.exit()
        # faire les tests ici
        return data
    
    else :
        # the url is not reachable
        # throw error
        logger.error(f"online data file is not reachable at {url}, code {r.status_code}")
        sys.exit()

if __name__ == '__main__':
    data = load_ctd(get_url("sola"))