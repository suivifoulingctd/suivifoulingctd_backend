#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 18 23:45:43 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""

import anomaly_count as ac
import database_manager as dbm
import pandas as pd
import tools as tl
import numpy as np
import html_templating2 as ht
from size import A4_paysage
import config
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import file_manager as fm

#buoy_name = "sola"

def build_report(buoy_name):
    last = dbm.get_last_recorded_date(buoy_name)
    begin7d = (last-pd.Timedelta("7D")).floor("1D").strftime("%Y-%m-%d %H:%M:%S")
    begin1d = last.floor("1D").strftime("%Y-%m-%d %H:%M:%S")
    end = (last.ceil("1D")-pd.Timedelta("1S")).strftime("%Y-%m-%d %H:%M:%S")
    slug = last.strftime("%Y%m%d%H%M%S")
    s_last = last.strftime("%Y-%m-%d")
    s_now = pd.Timestamp.now().strftime("%Y-%m-%d %H:%M:%S")
    
    data_7d = dbm.load_raw_data(buoy_name,begin=begin7d,end=end)
    globals()["data_7d"] = data_7d
    data_1d = dbm.load_raw_data(buoy_name,begin=begin1d,end=end)
    
    # drop badflags
    data_7d = data_7d.where(data_7d!=config.badflag)
    # drop startflags
    data_7d = data_7d.where(data_7d!=config.startflag)
    
    anom_7d = ac.count_anomalies(buoy_name,data_7d)
    anom_1d = ac.count_anomalies(buoy_name,data_1d)
    globals()["anom_7d"] = anom_7d
    
    # build indicator
    
    l = len(data_1d)
    indicator = anom_1d.sum_rejected.anoms/l*100
    indicator_names = [tl.col2nameonly(e) for e in indicator.index]
    indicator_values = np.around(indicator.values,decimals=1)
    indicator_colors = np.empty(indicator_values.shape,dtype="U6")
    indicator_colors[indicator_values<1] = "green"
    indicator_colors[(indicator_values>=1) & (indicator_values<5)] = "yellow" 
    indicator_colors[(indicator_values>=5) & (indicator_values<10)] = "orange" 
    indicator_colors[indicator_values>=10] = "red"
    s_indicator = ht.build_indicator_table(indicator_names, indicator_colors, indicator_values)
    
    print(s_indicator)
    
    # build details
    
    d_trad_filters = {
        "badflag" : "badflag",
        "startflag" : "startflag",
        "null" : "manquantes",
        "rstatic" : "impossibles",
        "rsl" : "accroissement",
        "rsp" : "pic",
        "rsk" : "adaptatif",
        "anoms" : "anomalies"
        }
    
    details = anom_1d.sum_rejected
    details = details.rename(d_trad_filters,axis=1)
    details = details.rename(tl.col2nameonly)
    s_details = details.to_html(classes="details")
    pinks = (indicator_values>=1)
    for b in pinks:
        if b:
            pass
            s_details = s_details.replace("<tr>","<tr class='pink'>",1)
        else :
            pass
            s_details = s_details.replace("<tr>","<tr class=''>",1)
    
    print(s_details)
    
    # build graph
    
    graph_public = fm.build_path(config.public_basepath,slug,ext=".png",absolute=False)
    graph_archive = fm.build_path(config.archive_basepath,slug,ext=".png",absolute=False)
    graph_web = fm.build_path(config.web_basepath,slug,ext=".png",absolute=False)
    
    keys = config.sensor_list[buoy_name]
    
    fig, axes = plt.subplots(len(keys),1,sharex=True)
    fig.set_size_inches(*A4_paysage)
    
    for i,key in enumerate(keys):
        axes[i].plot(data_7d.mdates,data_7d[key],c="k",label="Données brutes")
        axes[i].scatter(data_7d.mdates,data_7d[key].where(anom_7d.anoms[key]),label="Anomalies détectées",c="r",marker="x",zorder=20)
        axes[i].set_ylabel(tl.col2name(key))
        axes[i].grid(True)
        axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%d %b - %H:%M'))
        axes[i].xaxis.set_major_locator(mdates.HourLocator(interval=6))
        axes[i].legend(loc="upper left")
    
    fig.autofmt_xdate()
    fig.tight_layout(pad=0)
    fig.subplots_adjust(hspace=0)
    fig.savefig(graph_archive,bbox_inches='tight')
    fig.savefig(graph_public,bbox_inches='tight')
    
    s_graph = f"<img src='{graph_web}'></img>"
    
    print(s_graph)
    
    # build webpage
    
    html_params = {
        "working_date" : s_last,
        "update_date" : s_now,
        "free_space" : tl.disk_usage(),
        "indicator_table" : s_indicator,
        "graph_last" : s_graph,
        "details_table" : s_details
        }
    
    s_html = ht.html_template.format(**html_params)
    
    print(s_html)
    
    webpage_public = fm.build_path(config.public_basepath,"index",ext=".html",absolute=False)
    
    with open(webpage_public,"w") as f :
        f.write(s_html)
        
if __name__ == '__main__':
    build_report("sola")