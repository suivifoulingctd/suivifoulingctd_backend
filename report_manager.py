#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  4 17:07:35 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""

import numpy as np
import pandas as pd
import database_manager as dbm
import matplotlib.pyplot as plt
import tools as tl
from size import A4_paysage
import matplotlib.dates as mdates
import html_templating as ht
import file_manager as fm
import config

def build_report(buoy_name, data, anomaly):
    # extract
    last_anom = anom_7d.sort_values(by="datetime").iloc[-1]
    
    # get keys
    keys = config.sensor_list[buoy_name]
    
    
    # remove error codes
    data_7d[keys] = data_7d[keys][data_7d[keys]>-100]
    # shift anomaly for graph purpose 
    anom_7d.datetime = anom_7d.datetime+pd.Timedelta("12H")
    
    data_qc_7d = data_7d.set_index("datetime").join(qc_7d.set_index("datetime").add_prefix("qc_"))

    # add mdates
    anom_7d = tl.addmdates(anom_7d)
    # data_7d = tl.addmdates(data_7d)
    # qc_7d = tl.addmdates(qc_7d)
    
    data_qc_7d = tl.addmdates(data_qc_7d, index=True)
    
    
    # current working date (should be yesterday)
    str_date = data_7d.datetime.max().strftime("%Y-%m-%d")
    str_date_slug = data_7d.datetime.max().strftime("%Y%m%d")
    
    # build webpage
    
    # compute necessary values
    nb1d = 24*60
    t_anom = last_anom.drop("datetime").values/nb1d*100
    t_anom = t_anom.astype(float) 
    t_anom = np.around(t_anom,2)
    
    t_anom_med7d = anom_7d.drop(["datetime","mdates"],axis=1).median().values/nb1d*100
    t_anom_med7d = t_anom_med7d.astype(float) 
    t_anom_med7d = np.around(t_anom_med7d,2)
    
    t_anom_grad = t_anom - t_anom_med7d
    t_anom_grad = t_anom_grad.astype(float) 
    t_anom_grad = np.around(t_anom_grad,2)
    
    t_anom_color = np.empty(t_anom.shape,dtype="U6")
    t_anom_color[t_anom<1] = "green"
    t_anom_color[(t_anom>=1) & (t_anom<5)] = "yellow" 
    t_anom_color[(t_anom>=5) & (t_anom<10)] = "orange" 
    t_anom_color[t_anom>=10] = "red"
    
    print(t_anom_color)
    
    t_anom_grad_color = np.empty(t_anom.shape,dtype="U6")
    t_anom_grad_color[t_anom_grad<-.5] = "green"
    t_anom_grad_color[t_anom_grad>.5] = "red"
    t_anom_grad_color[np.abs(t_anom_grad)<.5] = "white"
    
    print(t_anom_grad_color)
    
    t_anom_grad_arrow = np.empty(t_anom.shape,dtype="U6")
    t_anom_grad_arrow[t_anom_grad<-.5] = "down"
    t_anom_grad_arrow[t_anom_grad>.5] = "up"
    t_anom_grad_arrow[np.abs(t_anom_grad)<.5] = "stable"
    
    print(t_anom_grad_arrow)
    
    # building table
    s_error = ""
    s_missing = ""
    
    for i in range(len(t_anom)):
        var_sign = ""
        if t_anom_grad[i] >= 0 :
            var_sign ="+"
        td_params_tmp = {
            "value_color" : t_anom_color[i],
            "value" : t_anom[i],
            "var_color" : t_anom_grad_color[i],
            "arrow" : ht.arrows[t_anom_grad_arrow[i]],
            "var" : f"{var_sign}{t_anom_grad[i]}"
            }
        if i<5 :
            s_error += ht.td_template.format(**td_params_tmp)
        else :
            s_missing += ht.td_template.format(**td_params_tmp)
    
    table_params_tmp = {
        "error" : s_error,
        "missing" : s_missing
        }
    s_table = ht.table_template.format(**table_params_tmp)
    
    # build graph fname
    public_basepath = config.public_basepath
    archive_basepath = config.archive_basepath
    web_basepath = config.web_basepath
    graph_archive = fm.build_path(archive_basepath,f"{str_date_slug}_graph_last7d",ext=".png")
    graph_public = fm.build_path(public_basepath,f"{str_date_slug}_graph_last7d",ext=".png")
    graph_web = fm.build_path(web_basepath,f"{str_date_slug}_graph_last7d",ext=".png",absolute=False)
    #detail_table = fm.build_path(config.archive_basepath,f"{str_date_slug}_rejected_counts",ext='.csv')
    detail_table = fm.build_path(config.archive_basepath,f"20220913_rejected_counts",ext='.csv')
    
    # graph 7 days
    fig, axes = plt.subplots(len(keys),1,sharex=True)
    fig.set_size_inches(*A4_paysage)
    
    print(data_qc_7d)
    
    # TODO inner join on datetime to fix issue
    
    for i,key in enumerate(keys):
        errkey = f"error_{key}"
        qckey = f"qc_{key}"
        axes[i].plot(data_qc_7d.mdates,data_qc_7d[key],label="données brutes",c="k")
        axes[i].scatter(data_qc_7d.mdates,data_qc_7d[key].where((data_qc_7d[qckey]!=1) & (data_qc_7d[qckey]!=9) & ~data_qc_7d[qckey].isnull()),label="anomalies détectées",c="r",marker="x",zorder=20)
        axes[i].set_ylabel(tl.col2name(key))
        axes[i].grid(True)
        axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%m-%d %H:%M'))
        axes[i].xaxis.set_major_locator(mdates.HourLocator(interval=6))
        axes[i].legend(loc="upper left")
        
        ax2 = axes[i].twinx()
        #ax2.plot(anom_7d.mdates,anom_7d[errkey]/nb1d*100,label="anomalies (%)",c="tab:orange", marker="P")
        ax2.step(anom_7d.mdates,anom_7d[errkey]/nb1d*100,label="anomalies (%)",c="tab:orange",marker="o",where="mid")
        #ax2.fill_between(anom_7d.mdates,anom_7d[errkey],alpha=0.4,color="tab:orange")
        ax2.set_ylabel("anomalies (%)")
        
        #nearset multiple of 5 over, set ylim
        anom_max = (anom_7d[errkey].values/nb1d*100).max()
        ymax = np.ceil(anom_max/5)*5
        ax2.set_ylim((0,ymax))
        
        ax2.legend(loc="lower left")
    
    fig.autofmt_xdate()
    fig.tight_layout(pad=0)
    fig.subplots_adjust(hspace=0)
    fig.savefig(graph_archive,bbox_inches='tight')
    fig.savefig(graph_public,bbox_inches='tight')
    
    s_graph = ht.img_template.format(img=graph_web)
    
    s_detail_table = pd.read_csv(detail_table,index_col=0).to_html()
    
    globals()["s_detail_table"]=s_detail_table
    
    # build html
    html_params = {
        "buoy_name" : buoy_name,
        "date" : str_date,
        "build_date" : pd.Timestamp.now().strftime("%Y-%m-%d %H:%M:%S"),
        "table" : s_table,
        "graph" : s_graph,
        "detail_table" : s_detail_table
        }
    
    s_html = ht.html_template.format(**html_params)
    
    webpage_public = fm.build_path(config.public_basepath,"index",ext=".html")
    
    with open(webpage_public,"w") as f :
        f.write(s_html)
        
if __name__ == '__main__' :
    buoy_name = sola
    build_report(buoy_name)
    
    # anom_7d = dbm.load_anomaly_data_7d(buoy_name)
    # data_7d = dbm.load_raw_data_7d(buoy_name)
    # qc_7d = dbm.load_qc_data_7d(buoy_name)
    
    # # extract
    # last_anom = anom_7d.sort_values(by="datetime").iloc[-1]
    
    # # get keys
    # datetime_col_name = "datetime"
    # keys = [key for key in list(data_7d.keys()) if ("mdates" not in key) and (datetime_col_name not in key)]
    
    # print(keys)
    
    # # remove error codes
    # data_7d[keys] = data_7d[keys][data_7d[keys]>-100]
    # # shift anomaly for graph purpose
    # anom_7d.datetime = anom_7d.datetime+pd.Timedelta("12H")
    
    # data_qc_7d = data_7d.set_index("datetime").join(qc_7d.set_index("datetime").add_prefix("qc_"))