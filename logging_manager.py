#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  4 15:11:10 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""

# https://docs.python.org/3/howto/logging.html#

import logging
import sys
import config
import os

# 'application' code
# logger.debug('debug message')
# logger.info('info message')
# logger.warning('warn message')
# logger.error('error message')
# logger.critical('critical message')

log_fname = os.path.join(config.log_basepath,"log")

def getlogger(module_name):
    # create logger
    logger = logging.getLogger(module_name)
    logger.setLevel(logging.DEBUG)
    
    # create console handler and set level to debug
    ch = logging.StreamHandler(stream=sys.stdout)
    ch.setLevel(logging.DEBUG)
    
    # create file handler and set the level to debug
    fh = logging.FileHandler(log_fname,encoding="utf8")
    fh.setLevel(logging.DEBUG)
    
    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    
    # add formatter to ch and fh
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    
    # clean previous handlers
    cleanhandlers(logger)
    
    # add ch and fh to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    
    return logger

def cleanhandlers(logger):
    nb_hdlr = len(logger.handlers)
    for i in range(nb_hdlr):
        logger.removeHandler(logger.handlers[0])