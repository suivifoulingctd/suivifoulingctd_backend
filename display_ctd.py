#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 02:37:30 2022

@author: Thierry Vala
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import tools as tl
from size import A4_paysage,A4_portrait,cm

def diplay(name,data):
    datetime_col_name="datetime"
    keys = [key for key in list(data.keys()) if ("mdates" not in key) and (datetime_col_name not in key)]
        
    fig, axes = plt.subplots(len(keys),1,sharex=True)
    fig.set_size_inches(*A4_paysage)
    
    for i,key in enumerate(keys):
        axes[i].plot(data.mdates,data[key],label=key,c=tl.color(i),zorder=1)
        axes[i].set_ylabel(key)
        axes[i].grid(True)
        axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
        axes[i].xaxis.set_major_locator(mdates.HourLocator())
        axes[i].legend(loc=4)
    
    fig.suptitle(f"{name} - {data.datetime[0].strftime('%Y-%m-%d')}")
    fig.autofmt_xdate()
    fig.tight_layout(pad=0)
    fig.subplots_adjust(hspace=0)
    
    #fname = fm.build_path(os.getcwd(),"_build","raw","full_graph_POEM_RAW_more_data",ext=".svg")
    #fig.savefig(fname,bbox_inches='tight')