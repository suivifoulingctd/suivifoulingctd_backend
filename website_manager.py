#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  5 20:07:04 2022

@author: thierry.vala@etudiant.univ-perp.fr
"""
import pandas as pd
import os
import config

wd = config.server_basepath

def gitlab_update():
    s_datetime = pd.Timestamp.now().strftime("%Y-%m-%d %H:%M:%S")
    commit_msg = f"Website update on {s_datetime}"
    cmd = f"cd {wd}; git add .; git commit -m '{commit_msg}'; git push origin master;";
    os.system(cmd)
    
if __name__ == '__main__' :
    gitlab_update()